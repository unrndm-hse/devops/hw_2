#!/usr/bin/env bash

# SubTask 3:  Map Reduce Awk
# ==========================
# You need to write a script,
# which emulates Map-Reduce task calculation using bash + awk.
# Also, you have to add a parameter, stating number of reducers, which are used in the process
# Arguments are passed the same way as in previous tasks

# NOTE
# ----
# added options for input and output

# init
INPUT="./input.txt"
OUTPUT="./out/"
NUM_REDUCERS=1
DEBUG=false
UNKNOWN=()

# args
while [[ $# -gt 0 ]]; do
  key=$1
  value=$2
#   echo $key $value
  case $key in
    -i|--input)
      INPUT=$value
      shift 2
    ;;
    -o|--output)
      OUTPUT=$value
      shift 2
    ;;
    -n|--num_reducers)
      NUM_REDUCERS=$value
      shift 2
    ;;
    -d|--debug)
      DEBUG=true
      shift
    ;;
    *)
      UNKNOWN+=("$key")
      shift
    ;;
  esac
done

if [ $DEBUG = "true" ]; then
  echo "input: $INPUT"
  echo "output: $OUTPUT"
  echo "num_reducers: $NUM_REDUCERS"
  echo "unknown: " "${UNKNOWN[@]}"
fi

# create out folder
mkdir -p "$OUTPUT"

# reduce
word2len=($(awk '{print $1 ":" length($1)}' $INPUT))
if [ $DEBUG = "true" ]; then
  echo "word to length: " "${word2len[@]}"
  echo "length of \`word to length\`: ${#word2len[@]}"
fi

# combine
max_word_len=0
declare -A hash_word2len
for w2l in "${word2len[@]}"; do
  IFS=':'
  declare -a wl=($w2l)
  word=${wl[0]}
  len=${wl[1]}
  hash_word2len["$word"]=$len
  if [ $len -gt $max_word_len ]; then
    max_word_len=$len
  fi
done

echo "${hash_word2len[@]@K}"

len2word=()

for i in $(seq $max_word_len); do
    i_arr=$(
    for key in "${!hash_word2len[@]}"; do
      if [[ "${hash_word2len[$key]}" = "$i" ]]; then
        echo "${key}"
      fi
    done
    )
    echo $i_arr
    len2word+=("$i_arr")
done

echo "${len2word[@]}"










