#!/usr/bin/env bash

# Task
# ====
# Create script that accepts file and outputs the line with file numbers.
# To enumerate line we can use:
# while IFS= read -r line; do COMMAND; done < input.file

# IMPLEMETATION
# -------------
# create var for line number, iterate over lines in file while printinf var and line and then increase line by 1 using strange but easy bash math


idx=0
while IFS= read -r line; do
    echo $idx $line;
    ((idx++))
done < $1
