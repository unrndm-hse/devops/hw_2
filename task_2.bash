#!/usr/bin/env bash

# SubTask 2: Train val split
# ==========================
# In this task you need to write a script that will divide a .csv dataset into train and validation samples. 
# Script requires 3 arguments:
# --input ... (path to the dataset)
# --train_ratio ... (percentage of objects in train sample)
# --y_column <column name> (name of the column, where objects class labels are located)

# NOTE
# ----
# added option to specify output folder

# IMPLEMETATION
# -------------
# get len of file, multiply by ration and get n, write first n lines to train, everuthing else to test in -o folder

# init
INPUT="./input.csv"
RATIO="0.5"
OUT="./out/"
Y_DEPRECATERD=false
UNKNOWN=()

#args
while [[ $# -gt 0 ]]; do
  key=$1
  value=$2
#   echo $key $value
  case $key in
    -i|--input)
      INPUT=$value
      shift 2
    ;;
    -r|-t|--train_ration)
      RATIO=$value
      shift 2
    ;;
    -o|--out)
      OUT=$value
      shift 2
    ;;
    -y|-c|--y_column)
      Y_DEPRECATERD="true"
      shift 2
    ;;
    *)
      UNKNOWN+=($value)
      shift
    ;;
  esac
done

mkdir -p $OUT

printf "input: $INPUT\nratio: $RATIO\ndeprecated: $Y_DEPRECATERD\nunknown: $UNKNOWN\n"

if [ $Y_DEPRECATERD = true ]; then
    echo "INFO: option --y_column (-y or -c) is deprecated and doesn't affect the output"
    echo "INFO: consider removing it to avoid future mistaces or confusion with new options"
fi

len=$(bc -l <<< "$(wc -l < "$INPUT")-1")
echo $len

train_len=$(printf "%.0f" $(bc -l <<< "$len * $RATIO"))
echo $train_len

header=$(head -n 1 $INPUT)

echo $header > $OUT/train.csv
echo $header > $OUT/test.csv

idx=0
while IFS= read -r line; do
    printf "$idx $train_len "
    if [[ "$idx" -lt "$train_len" ]]; then
        echo "train"
        echo $line >> $OUT/train.csv
    else
        echo "test"
        echo $line >> $OUT/test.csv
    fi
    ((idx++))
done <<< $(tail -n+2 $INPUT)



