HW 2
====

Notes
-----
0. general notes on realisation: I hate outputing files to root, so I added options to output to folder where they were none
1. tasks have 2 types of comments: debug out(left for fixing) and subtasks tasks in case someone actually reads this
2. task 3 (with asterisks) isn't complete because bash is stupid and I don't have eternity to do it, but the start is there
3. task 7 is from lecture, prof said that it'll be like home work, it's easy and I did it the same afternoon

Execution
---------
* task_7.bash:
```sh
./task_7.bash task_1.bash 
```
task from -
* task_1.bash:
```sh
./task_1.bash -f ./labelled_newscatcher_dataset.csv -c link -o ./out_folder/ -n 8
```
* task_2.bash:
```sh
./task_2.bash -i ./gender_submission.csv -c 1 -r 0.3
```
note: -c to veiw deprecation warning and ratio of .3 for not int division