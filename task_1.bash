#!/usr/bin/env bash

# SubTask 1: Parallel Dataset Processing
# ======================================
# In this task you need to write a script that will download some dataset in parallel.
# Script have to accept these arguments:
# - number of workers
# - the column in which the data links are located
# - folder for saving data
# All arguments must be named.
# Link to the dataset- https://drive.google.com/file/d/1EfRc2RLVdwWlXWz3nDIBEv_EvvOMd9ip/view?usp=sharing 

# NOTE
# ----
# added option for file input: `-f|--file`

# IMPLEMETATION
# -------------
# get header, get columns, get column idx, using awk get list of links, download links using parralel

# init
WORKERS=1
FILE="./input.csv"
COLUMN=""
OUT="./out/"
UNKNOWN=()

# args
while [[ $# -gt 0 ]]; do
  key=$1
  value=$2
#   echo $key $value
  case $key in
    -n|--num_workers)
      WORKERS=$value
      shift 2
    ;;
    -f|--file)
      FILE=$value
      shift 2
    ;;
    -c|--column)
      COLUMN=$value
      shift 2
    ;;
    -o|--out_dir)
      OUT=$value
      shift 2
    ;;
    *)
      UNKNOWN+=($value)
      shift
    ;;
  esac
done

# printf "workers: $WORKERS\nfile: $FILE\ncolumn: $COLUMN\nout_folder: $OUT\nunknown: $UNKNOWN\n"

# create out folder
mkdir -p $OUT

# csv header, maybe move to comand belove
header=$(head -n 1 $FILE)

# echo "header: $header"

# stopped working, second is strange but works
# IFS=';' read -ra columns <<< "$header"
columns=$(echo $header | tr ";" "\n")

# echo "cols: $columns"

# find number of column from name (starts from 1 bc awk is stupid)
selected_column=1
idx=1
for col in $columns; do
  # echo $col $idx
  if [ "$col" == "$COLUMN" ]; then
    selected_column=$idx
  fi
  ((idx++))
done
# echo $selected_column

# get all links, awk is still stupid but skipping first line (header, no date) is usefull ((tail can do the same, look in task_2.bash))
links=$(awk -F';' -v col=$selected_column 'NR>1 {print $col}' $FILE)

# for link in $links; do
  # echo $link
# done

# main call, redirect as file bc otherwise parrallel raises `too big` error, unix is strane, this is the least strange sollution
parallel -j $WORKERS --bar --gnu "wget -q -P $OUT {}" <<< "$links"
